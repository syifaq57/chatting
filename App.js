/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import React, {Component} from 'react';
import DrawerLayout from './src/DrawerLayout';
import {initApi} from './src/services/api';

export default class App extends Component {
  componentWillMount() {
    initApi();
  }

  render() {
    return <DrawerLayout />;
  }
}
