import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';
import Home from './View/Home/Home';
import Chat from './View/Chat/Chat';

// const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

// function TabNav() {
//   return (
//     <Tab.Navigator
//       screenOptions={({route}) => ({
//         tabBarIcon: ({color, size}) => {
//           let iconName;

//           if (route.name === 'Audio Player') {
//             iconName = 'play-circle-filled';
//           } else if (route.name === 'Play List') {
//             iconName = 'playlist-play';
//           }

//           // You can return any component that you like here!
//           return <Icon name={iconName} size={size} color={color} />;
//         },
//       })}
//       tabBarOptions={{
//         activeTintColor: 'black',
//         inactiveTintColor: 'gray',
//         activeBackgroundColor: colors.grayBG,
//         inactiveBackgroundColor: colors.grayBG,
//       }}>
//       <Tab.Screen name="Audio Player" component={MusicPlayer} />
//       <Tab.Screen name="Play List" component={PlayList} />
//     </Tab.Navigator>
//   );
// }

export default function DrawerLayout() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Home"
        screenOptions={{headerShown: false}}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Chat" component={Chat} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
