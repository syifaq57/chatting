import {StyleSheet} from 'react-native';

const defstyles = StyleSheet.create({
  Header: {
    backgroundColor: '#66DD87',
  },
  HeaderView: {
    flex: 1,
    flexDirection: 'row',
    // justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  HeaderText: {
    flex: 1,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
  },
  ImageHeader: {
    height: 30,
    width: 30,
    borderRadius: 20,
    marginHorizontal: 10,
  },
  HeaderBack: {
    flex: 1,
    paddingHorizontal: 15,
    flexDirection: 'row',
  },
  ButtonHeaderBack: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ButtonHeaderRight: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default defstyles;
