import React, {Component} from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {Header, Footer, Container, Content} from 'native-base';
import defstyles from '../../res/style/GlobalStyle';

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userdata: [
        {
          name: 'M. Syifaul Qulub',
          image: require('../../res/images/Foto.png'),
          table: 'messages',
        },
        {
          name: 'Mimin',
          image: require('../../res/images/mimin.jpeg'),
          table: 'messages2',
        },
      ],
    };
  }

  onPressUser(tableName, user, foto) {
    this.props.navigation.navigate('Chat', {
      tableName: tableName,
      user: user,
      foto: foto,
    });
  }

  render() {
    return (
      <Container>
        <Header style={defstyles.Header}>
          <View style={defstyles.HeaderView}>
            <Text style={defstyles.HeaderText}>Ngobrol Yuk</Text>
          </View>
        </Header>
        <Content>
          <View style={{flex: 1}}>
            {this.state.userdata.map((data, index) => (
              <TouchableOpacity
                onPress={() =>
                  this.onPressUser(data.table, data.name, data.image)
                }
                key={index}
                style={styles.listItem}>
                <View style={{flex: 1}}>
                  <Image source={data.image} style={styles.imageList} />
                </View>
                <View style={styles.nameView}>
                  <Text style={styles.nameList}>{data.name}</Text>
                  <Text style={styles.bacaList}>Baca Pesan</Text>
                </View>
              </TouchableOpacity>
            ))}
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  listItem: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    justifyContent: 'center',
    paddingVertical: 10,
    borderBottomWidth: 0.5,
    borderBottomColor: 'gray',
  },
  imageList: {
    height: 50,
    width: 50,
    borderRadius: 10,
  },
  nameView: {
    flex: 5,
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: 10,
  },
  nameList: {
    fontWeight: 'bold',
    fontSize: 17,
  },
  bacaList: {
    color: 'gray',
    marginLeft: 5,
    marginTop: 5,
  },
});
