import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Keyboard,
  TouchableOpacity,
  TextInput,
} from 'react-native';

class InputText extends React.Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  state = {
    text: '',
  };

  submit() {
    this.props.submit(this.state.text, this.props.table, this.props.incoming);

    this.setState({
      text: '',
    });

    Keyboard.dismiss();
  }

  render() {
    return (
      <View style={styles.inputtext}>
        <TextInput
          style={styles.Textarea}
          value={this.state.text}
          onChangeText={text => this.setState({text})}
          onSubmitEditing={event => this.submit()}
          editable={true}
          maxLength={300}
        />
        <TouchableOpacity style={styles.buttonSend} onPress={this.submit}>
          <Text style={{color: 'white'}}>Kirim</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  Textarea: {
    width: '80%',
    paddingHorizontal: 10,
    height: 40,
    backgroundColor: 'white',
    borderColor: '#979797',
    borderStyle: 'solid',
    borderWidth: 1,
  },
  inputtext: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    margin: 10,
  },
  buttonSend: {
    backgroundColor: '#3BA4F1',
    padding: 10,
  },
});

export default InputText;
