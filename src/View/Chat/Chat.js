import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  KeyboardAvoidingView,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Image,
  TouchableOpacity,
} from 'react-native';
import {Text, Header, Footer, Container, Content} from 'native-base';
import FontAwsomeIcon from 'react-native-vector-icons/FontAwesome';
import FontistoIcon from 'react-native-vector-icons/Fontisto';
import {getMessages, postMessage} from '../../services/api';
import defstyles from '../../res/style/GlobalStyle';
import Message from './Message';
import InputText from './InputText';

export default class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: [],
      isLoading: true,
      name: '',
      foto: '',
      status: 'Sender',
      iconstatus: 'checkbox-active',
      table: 'messages',
      incoming: true,
    };
  }

  async componentDidMount() {
    const {tableName, user, foto} = await this.props.route.params;
    this.unsubscribeGetMessages = getMessages(tableName, snapshot => {
      this.setState(
        {
          messages: Object.values(snapshot.val()),
          name: user,
          foto: foto,
          table: tableName,
        },
        function() {
          setTimeout(() => {
            this.scrollView.scrollToEnd();
          });
          this.setState({
            isLoading: false,
          });
        },
      );
    });
  }

  componentWillUnmount() {
    this.unsubscribeGetMessages();
  }

  onCheck() {
    if (this.state.status === 'Sender') {
      this.setState({
        status: 'Receiver',
        iconstatus: 'checkbox-passive',
        incoming: false,
      });
    }
    if (this.state.status === 'Receiver') {
      this.setState({
        status: 'Sender',
        iconstatus: 'checkbox-active',
        incoming: true,
      });
    }
  }

  render() {
    return (
      <Container>
        <Header style={defstyles.Header}>
          <View style={defstyles.HeaderBack}>
            <TouchableOpacity
              style={[defstyles.ButtonHeaderBack, {flex: 4}]}
              onPress={() => this.props.navigation.goBack()}>
              <FontAwsomeIcon name="chevron-left" size={20} color="white" />
              <Image source={this.state.foto} style={defstyles.ImageHeader} />
              <Text style={defstyles.HeaderText}>{this.state.name}</Text>
            </TouchableOpacity>
            {this.state.table !== 'messages' ? (
              <TouchableOpacity
                onPress={() => this.onCheck()}
                style={defstyles.ButtonHeaderRight}>
                <Text
                  style={{marginRight: 5, fontSize: 10, fontWeight: 'bold'}}>
                  {this.state.status}
                </Text>
                <FontistoIcon
                  name={this.state.iconstatus}
                  size={20}
                  color="white"
                />
              </TouchableOpacity>
            ) : (
              <View />
            )}
          </View>
        </Header>
        <Content>
          {this.state.isLoading == true ? (
            <View style={{alignItems: 'center'}}>
              <ActivityIndicator size={'large'} />
            </View>
          ) : (
            <ScrollView
              style={styles.container}
              ref={ref => {
                this.scrollView = ref;
              }}>
              {this.state.messages.map((item, index) => (
                <Message incoming={item.incoming} message={item.message} />
              ))}
            </ScrollView>
          )}
        </Content>
        <Footer style={{backgroundColor: '#66DD87'}}>
          <InputText
            submit={postMessage}
            table={this.state.table}
            incoming={this.state.incoming}
          />
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  listItem: {
    width: '70%',
    margin: 10,
    padding: 10,
    backgroundColor: 'white',
    borderColor: '#979797',
    borderStyle: 'solid',
    borderWidth: 1,
    borderRadius: 10,
  },
  incomingMessage: {
    alignSelf: 'flex-end',
    backgroundColor: '#E1FFC7',
  },
});
