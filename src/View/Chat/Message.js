import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';

class Message extends Component {
  render() {
    return (
      <View>
        {this.props.incoming === true ? (
          <View style={[styles.message, styles.incomingMessage]}>
            <Text>{this.props.message}</Text>
          </View>
        ) : (
          <View style={[styles.message]}>
            <Text>{this.props.message}</Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = {
  message: {
    width: '70%',
    margin: 10,
    padding: 10,
    backgroundColor: 'white',
    borderColor: '#979797',
    borderStyle: 'solid',
    borderWidth: 0.5,
    borderRadius: 5,
  },
  incomingMessage: {
    alignSelf: 'flex-end',
    backgroundColor: '#66DD87',
  },
};

export default Message;