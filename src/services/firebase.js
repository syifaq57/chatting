import * as firebase from 'firebase';

export const initialize = () =>
  firebase.initializeApp({
    apiKey: 'AIzaSyDKXj4Vjf8__rplCpG3ZOUyRLU5ca4z870',
    authDomain: 'chatting-a38c2.firebaseapp.com',
    databaseURL: 'https://chatting-a38c2.firebaseio.com',
    projectId: 'chatting-a38c2',
    storageBucket: 'chatting-a38c2.appspot.com',
    messagingSenderId: '901831482766',
    appId: '1:901831482766:web:f9a52fe7bac0d02b04d8df',
    measurementId: 'G-EMLP7VJ565',
  });

export const setListener = (endpoint, updaterFn) => {
  firebase
    .database()
    .ref(endpoint)
    .on('value', updaterFn);
  return () =>
    firebase
      .database()
      .ref(endpoint)
      .off();
};

export const pushData = (endpoint, data) => {
  return firebase
    .database()
    .ref(endpoint)
    .push(data);
};
