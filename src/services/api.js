import {initialize, setListener, pushData} from './firebase';

export const initApi = () => initialize();

export const getMessages = (table, updaterFn) => setListener(table, updaterFn);

export const postMessage = (message, table, incoming) => {
  if (Boolean(message)) {
    pushData(table, {
      incoming: incoming,
      message,
    });
  }
};
